# Generated by go2rpm 1.3
%bcond_without check

# https://github.com/emersion/go-textwrapper
%global goipath         github.com/emersion/go-textwrapper
%global commit          65d896831594eccd3e87d83b845e25d3580e5a39

%gometa

%global common_description %{expand:
A writer that wraps long text lines to a specified length.}

%global golicenses      LICENSE
%global godocs          README.md

Name:           %{goname}
Version:        0
Release:        0.1%{?dist}
Summary:        A writer that wraps long text lines to a specified length

License:        MIT
URL:            %{gourl}
Source0:        %{gosource}

%description
%{common_description}

%gopkg

%prep
%goprep

%install
%gopkginstall

%if %{with check}
%check
%gocheck
%endif

%gopkgfiles

%changelog
* Wed May 26 12:46:52 CEST 2021 Mathieu Trossevin <mathieu.trossevin@gmail.com> - 0-0.1.20210526git65d8968
- Initial package

